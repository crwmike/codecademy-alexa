## codecademy-alexa
# Build Your First Alexa Skill

In this lesson, you'll build a *Hello Codecademy* skill.

[Alexa GitHub page](https://github.com/alexa/alexa-skills-kit-sdk-for-nodejs)

[Node.js open source libraries](https://www.npmjs.com/)

#### BUILD YOUR FIRST ALEXA SKILL
## What is Alexa?
You may have heard of *Amazon Echo*, the voice-enabled speaker from Amazon that allows you to get things done, by using your voice.

The brain behind Echo and other Amazon voice-enabled devices like Echo Show, Echo Dot, and Amazon Tap is *Alexa* — the cloud based service that handles all the speech recognition, machine learning, and Natural Language Understanding for all Alexa enabled devices.

Alexa provides a set of built-in capabilities, referred to as *skills*, that define how you can interact with the device. For example, Alexa’s built-in skills include playing music, reading the news, getting a weather forecast, and querying Wikipedia. So, you could say things like:
```
Alexa, play Michael Jackson

Alexa, what's the weather in New York
```
In addition to these built-in skills, you can program custom skills by using the *Alexa Skills Kit (ASK)*. An Alexa user can then access these new abilities by asking Alexa questions or making requests.

In this lesson, you will create an Amazon developer account and use it to create your own skill!

---
#### INTENTREQUEST

When the user says "Alexa, tell code academy hello", Alexa recognizes that a specific intent is being requested (IntentRequest), and using the sample utterances we provided, maps it to HelloIntent.

#### LAUNCHREQUEST

If however, the user invokes the skill with the invocation name, but does not provide any command mapping to an intent, like "Alexa, open code academy", our skill will receive a LaunchRequest, instead of "IntentRequest".


You can read more about the Lambda Function Handler for Node.js at [docs.aws.amazon.com](http://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-handler.html).
